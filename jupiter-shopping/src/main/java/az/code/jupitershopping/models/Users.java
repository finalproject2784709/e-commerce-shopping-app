package az.code.jupitershopping.models;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Entity
@Data
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String lastName;
    @Column(name = "phone_number")
    private long phoneNumber;
    private String email;
    private String password;
    private String gender;
    @Column(name = "last_login_date")
    private LocalDate lastLoginDate;
    @Column(name = "account_creation_date")
    private LocalDate accountCreationDate;
    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @OneToOne
    @JoinColumn(name = "payment_information_id",referencedColumnName = "id")
    private  PaymentInformation paymentInformation;
    @OneToOne
    @JoinColumn(name = "address_id",referencedColumnName = "id")
    private Address address;
    @OneToMany(mappedBy = "users")
    private List<Product> product;
    @OneToMany(mappedBy = "users")
    private List<Orders> orders;

}
