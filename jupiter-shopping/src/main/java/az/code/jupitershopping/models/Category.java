package az.code.jupitershopping.models;

import jakarta.persistence.*;
import lombok.Data;
import java.util.*;
@Entity
@Data
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String description;
    private String name;
    @OneToMany(mappedBy = "category")
    private List<Product> product;

}
