package az.code.jupitershopping.models;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Entity
@Data
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private long id;
    private long quantity;
    @Column(name = "create_date")
    private LocalDate createDate;
    @Column(name = "is_checked_out")
    private boolean isCheckedOut;
    @Column(name = "total_price")
    private long totalPrice;
    @Column(name = "update_date")
    private LocalDate updateDate;
    @OneToMany(mappedBy = "cart")
    private List<Product> products;
    //user_id
}
