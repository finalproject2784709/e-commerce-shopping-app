package az.code.jupitershopping.models;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class OrderItems {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private long price;
    private long quantity;
    @Column(name = "total_price")
    private long totalPrice;
     @ManyToOne
     @JoinColumn(name = "order_id",referencedColumnName = "id")
     private Orders order;
    //product_id
}
