package az.code.jupitershopping.models;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Entity
@Data
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "order_date")
    private LocalDate orderDate;
    @Column(name = "order_status")
    private String orderStatus;
    private String policy;
    @Column(name = "shipping_inforations")
    private String shippingInforations;
    @Column(name = "total_amount")
    private long totalAmount;
    @OneToMany(mappedBy = "order")
    private List<OrderItems> orderItems;
    @OneToOne
    @JoinColumn(name = "payment_information_id",referencedColumnName = "id")
    private PaymentInformation paymentInformation;
    @ManyToOne
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private Users users;
}
