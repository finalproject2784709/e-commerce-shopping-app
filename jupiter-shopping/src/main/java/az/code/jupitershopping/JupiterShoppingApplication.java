package az.code.jupitershopping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JupiterShoppingApplication {

	public static void main(String[] args) {
		SpringApplication.run(JupiterShoppingApplication.class, args);
	}

}
