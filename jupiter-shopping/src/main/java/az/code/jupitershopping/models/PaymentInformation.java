package az.code.jupitershopping.models;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Entity
@Data
public class PaymentInformation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    @Column(name = "card_number")
    private long cardNumber;
    @Column(name = "card_cvv")
    private int cardCVV;
    @Column(name = "card_expiration_date")
    private LocalDate card_expiration_date;
    @Column(name = "payment_date")
    private LocalDate paymentDate;
    @Column(name = "payment_amount")
    private long paymentAmount;
    @Column(name = "payment_method")
    private String  paymentMethod;
    @OneToOne(mappedBy = "paymentInformation")
    private Users users;
    @OneToOne(mappedBy = "paymentInformation")
    private Orders orders;
}
