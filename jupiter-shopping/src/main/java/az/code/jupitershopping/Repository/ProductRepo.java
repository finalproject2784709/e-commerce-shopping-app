package az.code.jupitershopping.Repository;

import az.code.jupitershopping.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepo extends JpaRepository<Product, Long> {
}
