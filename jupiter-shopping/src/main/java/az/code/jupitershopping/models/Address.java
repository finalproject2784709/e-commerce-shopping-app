package az.code.jupitershopping.models;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String address_line1;
    private String city;

    @OneToOne(mappedBy = "address")
    private Users users;
}
